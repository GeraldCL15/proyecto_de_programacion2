/*
 */
package View_Proyect;

import Controller.LoginController;
import Model_Proyect.Seguridad;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * @author ycruz
 */
public class Login extends javax.swing.JFrame {

    private static Scanner sc;
    private int intentos;

    String user, pass;

    public Login() {
        initComponents();
        LoginController();
        this.setLocationRelativeTo(null);
      //  setSize(400, 550);
        setResizable(false);// para evitar que el Login se pueda agrandar o minimizar al gusto del usuario
        setTitle("System  Accesses");

        //Para agregar el fondo del loging 
        ImageIcon wallpaper = new ImageIcon("src/imagenes/wallpaperPrincipal.jpg");
        Icon icono = new ImageIcon(wallpaper.getImage().getScaledInstance(jLabel_Wallpaper.getWidth(), jLabel_Wallpaper.getHeight(), Image.SCALE_SMOOTH));
        jLabel_Wallpaper.setIcon(icono);
        this.repaint();

        //para agregar el logo al login
        ImageIcon wallpaper_logo = new ImageIcon("src/imagenes/GeraldSystem.png");
        Icon icono_logo = new ImageIcon(wallpaper_logo.getImage().getScaledInstance(jLabel_Logo.getWidth(), jLabel_Logo.getHeight(), Image.SCALE_DEFAULT));
        jLabel_Logo.setIcon(icono_logo);
        this.repaint();
        


    }

    public void LoginController() {
        LoginController loginC = new LoginController(this);
        ButtonAceder.addActionListener(loginC);

        List<Seguridad> listas = new ArrayList<>();
        Seguridad s = new Seguridad();
        s.setName("Gerald");
        s.setPassword("G123");
        listas.add(s);             /// 
        loginC.CrearArchivo(listas, false);
    }

    public Seguridad getUser() {
        Seguridad s = new Seguridad();
        s.setName(txt_user.getText().trim());
        s.setPassword(txt_password.getText());
        return s;
    }
    
    public void LoginVisible(boolean v){
        setVisible(v);
    }

    //cambio del icono del jframe
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("imagenes/icon.png"));
        return retValue;

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel_Logo = new javax.swing.JLabel();
        txt_user = new javax.swing.JTextField();
        txt_password = new javax.swing.JPasswordField();
        ButtonAceder = new javax.swing.JButton();
        jLabel_Footer = new javax.swing.JLabel();
        jLabel_Wallpaper = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(jLabel_Logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 0, 210, 180));

        txt_user.setBackground(new java.awt.Color(153, 153, 255));
        txt_user.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txt_user.setForeground(new java.awt.Color(0, 0, 0));
        txt_user.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_user.setToolTipText(" Please enter your username");
        txt_user.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(txt_user, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 220, 210, -1));

        txt_password.setBackground(new java.awt.Color(153, 153, 255));
        txt_password.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txt_password.setForeground(new java.awt.Color(0, 0, 0));
        txt_password.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_password.setToolTipText(" Please Enter your password");
        txt_password.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(txt_password, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 260, 210, -1));

        ButtonAceder.setBackground(new java.awt.Color(153, 153, 255));
        ButtonAceder.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        ButtonAceder.setText("Log in");
        ButtonAceder.setActionCommand("Ingresar");
        ButtonAceder.setBorder(null);
        ButtonAceder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonAcederActionPerformed(evt);
            }
        });
        getContentPane().add(ButtonAceder, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 310, 210, 35));

        jLabel_Footer.setText("Created By Gerald Cruz Laguna ® ");
        getContentPane().add(jLabel_Footer, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 380, -1, -1));
        getContentPane().add(jLabel_Wallpaper, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 378, 422));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ButtonAcederActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonAcederActionPerformed

    }//GEN-LAST:event_ButtonAcederActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonAceder;
    private javax.swing.JLabel jLabel_Footer;
    private javax.swing.JLabel jLabel_Logo;
    private javax.swing.JLabel jLabel_Wallpaper;
    private javax.swing.JPasswordField txt_password;
    private javax.swing.JTextField txt_user;
    // End of variables declaration//GEN-END:variables

}
