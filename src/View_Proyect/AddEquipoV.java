/*

 */
package View_Proyect;

import Controller.AddequipoController;
import Model_Proyect.AddEquipoModels;
import java.util.ArrayList;
import java.util.List;
import javax.swing.WindowConstants;

/**
 *
 *  @author ycruz
 */
public class AddEquipoV extends javax.swing.JFrame {

    public AddEquipoV() {
        initComponents();
        setSize(520, 330);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        InitAddEquipoController();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
    
     public void InitAddEquipoController() {
        AddequipoController c = new AddequipoController(this);
        jButton_GuardarEquipo.addActionListener(c);
    }

    public List<AddEquipoModels> getEquipo() {
        List<AddEquipoModels> listas = new ArrayList<>();
        AddEquipoModels equipo = new AddEquipoModels();
        equipo.setNameCliente(txt_NombreCliente.getText().trim());
        equipo.setMarcaEquipo(txt_MarcaEquipo.getText().trim());
        equipo.setEstado(jComboBox1.getSelectedItem().toString());
        equipo.setProblemasResolver(txarea_Problemas.getText().trim());
        equipo.setTelefone(txt_tefefono.getText().trim());
        listas.add(equipo);
        return listas;
    }  

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        txt_NombreCliente = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_tefefono = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        txt_MarcaEquipo = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txarea_Problemas = new javax.swing.JTextArea();
        jButton_GuardarEquipo = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel6.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Nombre del Cliente:");

        txt_NombreCliente.setBackground(new java.awt.Color(153, 153, 255));
        txt_NombreCliente.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txt_NombreCliente.setForeground(new java.awt.Color(0, 0, 0));
        txt_NombreCliente.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_NombreCliente.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Teléfono del Cliente:");

        txt_tefefono.setBackground(new java.awt.Color(153, 153, 255));
        txt_tefefono.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txt_tefefono.setForeground(new java.awt.Color(0, 0, 0));
        txt_tefefono.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_tefefono.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Marca del equipo:");

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Estado del equipo:");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "En Revisión", "Listo" }));

        txt_MarcaEquipo.setBackground(new java.awt.Color(153, 153, 255));
        txt_MarcaEquipo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        txt_MarcaEquipo.setForeground(new java.awt.Color(0, 0, 0));
        txt_MarcaEquipo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txt_MarcaEquipo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Problema resuelto o a resolver:");

        txarea_Problemas.setBackground(new java.awt.Color(153, 153, 250));
        txarea_Problemas.setColumns(20);
        txarea_Problemas.setForeground(new java.awt.Color(0, 0, 0));
        txarea_Problemas.setRows(5);
        jScrollPane1.setViewportView(txarea_Problemas);

        jButton_GuardarEquipo.setBackground(new java.awt.Color(153, 153, 255));
        jButton_GuardarEquipo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/GuardarE.png"))); // NOI18N
        jButton_GuardarEquipo.setActionCommand("Guardar");
        jButton_GuardarEquipo.setBorder(null);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 460, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel6)
                            .addGap(186, 186, 186)
                            .addComponent(jLabel4))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(txt_NombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(160, 160, 160)
                            .addComponent(txt_tefefono, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addGap(197, 197, 197)
                            .addComponent(jLabel5))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(txt_MarcaEquipo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(160, 160, 160)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jLabel1)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(90, 90, 90)
                            .addComponent(jButton_GuardarEquipo, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel6)
                        .addComponent(jLabel4))
                    .addGap(3, 3, 3)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txt_NombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_tefefono, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(30, 30, 30)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel2)
                        .addComponent(jLabel5))
                    .addGap(3, 3, 3)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txt_MarcaEquipo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(24, 24, 24)
                    .addComponent(jLabel1)
                    .addGap(3, 3, 3)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton_GuardarEquipo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_GuardarEquipo;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txarea_Problemas;
    private javax.swing.JTextField txt_MarcaEquipo;
    private javax.swing.JTextField txt_NombreCliente;
    private javax.swing.JTextField txt_tefefono;
    // End of variables declaration//GEN-END:variables
}
