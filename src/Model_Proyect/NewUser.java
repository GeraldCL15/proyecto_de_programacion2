/*
 */
package Model_Proyect;

/**
 * @author ycruz
 */
public class NewUser extends Seguridad {

    private String userName, correo, tefenone, tipoNivel;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTefenone() {
        return tefenone;
    }

    public void setTefenone(String tefenone) {
        this.tefenone = tefenone;
    }

    public String getTipoNivel() {
        return tipoNivel;
    }

    public void setTipoNivel(String tipoNivel) {
        this.tipoNivel = tipoNivel;
    }
}
