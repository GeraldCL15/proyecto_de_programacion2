/*
 */
package Model_Proyect;

/**
 * @author ycruz
 */
public class AddEquipoModels {

    private String nameCliente , telefone, marcaEquipo, estado, problemasResolver;

    public String getNameCliente() {
        return nameCliente;
    }

    public void setNameCliente(String nameCliente) {
        this.nameCliente = nameCliente;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getMarcaEquipo() {
        return marcaEquipo;
    }

    public void setMarcaEquipo(String marcaEquipo) {
        this.marcaEquipo = marcaEquipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getProblemasResolver() {
        return problemasResolver;
    }

    public void setProblemasResolver(String problemasResolver) {
        this.problemasResolver = problemasResolver;
    }   
}
