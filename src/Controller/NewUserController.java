/*
 */
package Controller;

import Model_Proyect.NewUser;
import View_Proyect.RegistroUser;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * @author ycruz
 */
public class NewUserController implements ActionListener {

    private RegistroUser regitroUser;

    public NewUserController(){
        
    }
    
    public NewUserController(RegistroUser regitroUser) {
        this.regitroUser = regitroUser;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean b = regitroUser.Validar();
        if (b) {
            CrearArchivo(regitroUser.getNewUser(), true);
        } else {
            JOptionPane.showMessageDialog(null, "Debe de Completar todos los campos..!!");
        }

    }

    public void CrearArchivo(List<NewUser> listas, boolean v) {

        try {
            FileWriter archivo = new FileWriter("Registro.txt", v);
            for (NewUser user : listas) {
                archivo.write(user.getName() + "," + user.getPassword() + "," + user.getUserName()
                        + "," + user.getCorreo() + "," + user.getTefenone() + "," + user.getTipoNivel() + "\n");
            }
            archivo.close();
            JOptionPane.showMessageDialog(null, "Se guardo correctamente");
        } catch (IOException e) {
        }
    }

    public List<NewUser> LeerArchivo() {
        File archivo = new File("Registro.txt");
        List<NewUser> user = new ArrayList<>();
        try {
            Scanner s = new Scanner(archivo);
            while (s.hasNext()) {
                String linia = s.nextLine();
                Scanner delimitar = new Scanner(linia); // 
                delimitar.useDelimiter("\\s*,\\s*");
                NewUser newUser = new NewUser();
                newUser.setName(delimitar.next());
                newUser.setPassword(delimitar.next());
                newUser.setUserName(delimitar.next());
                newUser.setCorreo(delimitar.next());
                newUser.setTefenone(delimitar.next());
                newUser.setTipoNivel(delimitar.next());
                user.add(newUser);
            }
            s.close();
        } catch (FileNotFoundException f) {
        }
        return user;
    }
}
