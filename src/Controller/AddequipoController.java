/*
 */
package Controller;

import Model_Proyect.AddEquipoModels;
import View_Proyect.AddEquipoV;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * @author ycruz
 */
public class AddequipoController implements ActionListener {

    private AddEquipoV addequipo;

    public AddequipoController() {

    }

    public AddequipoController(AddEquipoV addequipo) {
        this.addequipo = addequipo;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()) {

            case "Guardar":
                CrearArchivo(addequipo.getEquipo(), true);
                break;
        }
    }

    public void CrearArchivo(List<AddEquipoModels> listas, boolean v) {

        try {
            FileWriter archivo = new FileWriter("equipo.txt", v);
            for (AddEquipoModels user : listas) {
                archivo.write(user.getNameCliente() + "," + user.getMarcaEquipo() + "," + user.getEstado()
                        + "," + user.getProblemasResolver() + "," + user.getTelefone() + "\n");
            }
            archivo.close();
            JOptionPane.showMessageDialog(null, "Se guardo correctamente");
        } catch (IOException e) {
        }
    }

    public List<AddEquipoModels> LeerArchivo() {
        File archivo = new File("equipo.txt");
        List<AddEquipoModels> user = new ArrayList<>();
        try {
            Scanner s = new Scanner(archivo);
            while (s.hasNext()) {
                String linia = s.nextLine();
                Scanner delimitar = new Scanner(linia); // 
                delimitar.useDelimiter("\\s*,\\s*");
                AddEquipoModels equipo = new AddEquipoModels();
                equipo.setNameCliente(delimitar.next());
                equipo.setMarcaEquipo(delimitar.next());
                equipo.setEstado(delimitar.next());
                equipo.setProblemasResolver(delimitar.next());
                equipo.setTelefone(delimitar.next());
                user.add(equipo);
            }
            s.close();
        } catch (FileNotFoundException f) {
        }
        return user;
    }
}
