/*
 */
package Controller;

import Model_Proyect.Seguridad;
import View_Proyect.Login;
import View_Proyect.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import Model_Proyect.NewUser;

/**
 * @author ycruz
 */
public class LoginController implements ActionListener {

    private Login login;
    private NewUserController userC;

    public LoginController(Login login) {
        this.login = login;
        userC = new NewUserController();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Ingresar":
                switch(ValidarDate2(userC.LeerArchivo(),login.getUser())){
                    case "Administrador":
                        MainFrame m = new MainFrame(true);
                        break;
                    default:
                         MainFrame n = new MainFrame(false);
                        break;
                }
                break;
        }
    }

    public void CrearArchivo(List<Seguridad> listas, boolean v) {
        try {
            FileWriter archivo = new FileWriter("UserLogin.txt", v);
            for (Seguridad user : listas) {
                archivo.write(user.getName() + "," + user.getPassword() + "\n");
            }
            archivo.close();
        } catch (IOException e) {
        }
    }

    public List<Seguridad> LeerArchivo() {
        File archivo = new File("UserLogin.txt");
        List<Seguridad> user = new ArrayList<>();
        try {
            Scanner s = new Scanner(archivo);
            while (s.hasNext()) {
                String linia = s.nextLine();
                Scanner delimitar = new Scanner(linia);
                delimitar.useDelimiter("\\s*,\\s*");
                Seguridad seguridad = new Seguridad();
                seguridad.setName(delimitar.next());
                seguridad.setPassword(delimitar.next());
                user.add(seguridad);
            }
            s.close();
        } catch (FileNotFoundException f) {
            System.out.println("Error no existe el archivo");
            System.exit(1);
        }
        return user;
    }

    public boolean ValidarDate(List<Seguridad> listas, Seguridad user) {
        return listas.stream().anyMatch(s -> (s.getName().equals(user.getName()) && s.getPassword().equals(user.getPassword())));
    }

    public String ValidarDate2(List<NewUser> listas, Seguridad user) {
        for (NewUser u : listas) {
            if (u.getUserName().equals(user.getName()) && u.getPassword().equals(user.getPassword())) {
                return u.getTipoNivel();
            }
        }
        return "No";
    }
}
