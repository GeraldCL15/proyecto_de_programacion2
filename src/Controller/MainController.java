/*
 */
package Controller;

import View_Proyect.AddEquipoV;
import View_Proyect.MainFrame;
import View_Proyect.MostrarUser;
import View_Proyect.RegistroUser;
import java.awt.event.ActionEvent;
import View_Proyect.TablaEquipo;
import java.awt.event.ActionListener;

/**
 * @author David_Moody
 */
public class MainController implements ActionListener {

    private MainFrame mainFrame;

    public MainController(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "RegistroUser":
                RegistroUser r = new RegistroUser();
                break;
            case "GestionUser":
                NewUserController n = new NewUserController();
                MostrarUser m = new MostrarUser();
                m.llenarJTable(n.LeerArchivo());
                break;
            case "Secretario":
                AddEquipoV a = new AddEquipoV();
                break;
            case "Table":
                TablaEquipo tble = new TablaEquipo();
                AddequipoController con = new AddequipoController();
                tble.llenarJTable(con.LeerArchivo());
                break;
        }
    }
}
